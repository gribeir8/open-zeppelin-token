// contracts/Box.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

import "./Box.sol";

contract BoxV2 is Box {

    // Increments the stored value by 1
    function increment() public onlyOwner {
        _value = _value + 1;
        emit ValueChanged(_value);
    }
}