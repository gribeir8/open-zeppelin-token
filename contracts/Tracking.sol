// contracts/Box.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/extensions/ERC721URIStorageUpgradeable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";


contract Tracking is OwnableUpgradeable, ERC721URIStorageUpgradeable {

    //private props
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;

    //public props
    string public tracking_name;
    mapping(address => bool) public members;
    mapping(string => bool) public metadatae;
    //mapping of items to IPFS documents, forms, attachments, photos
    mapping(uint256 => string[]) public itemsToMetadata;

    struct Member{
        string name;
        address _address;
    }
    Member[] public allMembers;
    mapping(address => Member) private addressToMember;

    struct ItemRequest{
        
        string name;
        string description;
        string metadata;
        bool valid;
        uint256[] items;
    }    
    ItemRequest[] public requests;
    mapping(uint256 => ItemRequest) public itemsToRequest;

    struct TrackingData{
        address member;
        uint256 timestamp;
        string _event;
        string attachment;
    }
    mapping(uint256 => TrackingData[]) public itemsToTrackingData; //item, tuple(address,timestamp)

    function initialize(string memory _name) public initializer {
        __Ownable_init_unchained();
        __ERC721_init_unchained("Tracking", "TRC");
        tracking_name = _name;
    }

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor() initializer {}

    //base URI for the tokenURI (baseURI + tokenID);
    function _baseURI() override internal view virtual returns (string memory) {

        //using set token URI
        return "";// "http://www.example.com/tokens/";
    }

    function setTrackingName(string memory _name) public onlyOwner{

        tracking_name = _name;
    }

    event MemberAdded(address member);

    function addMember(string memory name, address _address) public onlyOwner {

        require(!members[_address]
            ,"Member already added!");
        Member memory member = Member(
            {
                name: name
                ,_address: _address
            }
        );
        allMembers.push(member);
        addressToMember[_address]=member;
        members[_address] = true;
        emit MemberAdded(_address);
    }

    //return private prop to owner
    function getAllMembers() public view onlyOwner returns(Member[] memory){

        return allMembers;
    }

    function getCurrentMember() public view isMember returns(Member memory){

        return addressToMember[msg.sender];
    }

    modifier isMember(){

        require(members[msg.sender]
                ,"Tracking: Not a member!");
        _;
    }

    event RequestCreated(uint id, string name);

    //tracking request created by contract owner
    function createRequest(        
        string memory _name
        ,string memory _description
        ,string memory _metadata) public onlyOwner{

        //request metadata is optional
        if((bytes(_metadata)).length != 0){

            require(!metadatae[_metadata]
                    ,"Metadata already registered!");
            metadatae[_metadata] = true;
        }

        ItemRequest memory request = ItemRequest({
            name: _name
            ,description: _description
            ,metadata: _metadata
            ,valid: true
            ,items: new uint256[](0)
        });
        
        requests.push(request);
        emit RequestCreated(requests.length-1, _name);
    }

    function invalidateRequest(uint requestIndex) public onlyOwner{

        ItemRequest storage request = requests[requestIndex];
        require(request.valid
                ,"Request not valid!");
        request.valid = false;
    }

    function getRequests() public view returns(ItemRequest[] memory){

        return requests;
    }

    event ItemCreated(uint requestId, uint256 itemId);

    //token item created by member, set to request
    function createItem(       
        uint requestIndex
        ,string memory _metadata) public isMember returns (uint256){

        ItemRequest storage request = requests[requestIndex];
        require(request.valid
                ,"Request not valid!");
        
        //ERC721 code
        _tokenIds.increment();
        uint256 newItemId = _tokenIds.current();
        _mint(msg.sender, newItemId);

        //save token to request
        request.items.push(newItemId);
        //maps token to request
        itemsToRequest[newItemId] = request;

        //item metadata is optional
        if((bytes(_metadata)).length != 0){

            //set metadata and register
            _setTokenURI(newItemId, _metadata);
            privateAddMetadata(newItemId, _metadata);
        }

        emit ItemCreated(requestIndex, newItemId);
        return newItemId;
    }

    event MetadataAdded(uint itemId, string metadata);
    
    /**
        Caller MUST be item current owner
     */
    function addMetadata(uint256 itemId, string memory metadata) public isMember{

        require(ownerOf(itemId)==msg.sender
                ,"Member is not current owner of Item!");
        //check valid request
        ItemRequest memory request = itemsToRequest[itemId];
        require(request.valid
                ,"Request not valid!");

        privateAddMetadata(itemId, metadata);
        emit MetadataAdded(itemId, metadata);
    }

    //registers item metadata to mappings
    function privateAddMetadata(uint256 itemId, string memory metadata) private{

        require( (bytes(metadata)).length != 0
                ,"Metadata cannot be empty string!");
        //metadata can only be set once
        require(!metadatae[metadata]
                ,"Metadata already registered!");
        
        itemsToMetadata[itemId].push(metadata);
        metadatae[metadata] = true;
        scanItem(itemId, "MetadataAdded", metadata);
    }

    function getMetadata(uint256 itemId) view public returns(string[] memory){

        return itemsToMetadata[itemId];
    }

    event ItemTransfered(uint256 itemId
                    ,address from, address to);

    /**
        Transfer item from current owner to other member of chain
     */
    function transferItem(uint256 itemId, address member) public isMember{

        require(members[member]
                ,"New owner not a member!");
        transferFrom(msg.sender, member, itemId);
        scanItem(itemId, "ItemTransfered", "");
        emit ItemTransfered(itemId, msg.sender, member);
    }

    event ItemScanned(uint256 itemId, address member);

    function scanItemByMember(uint256 itemId) public isMember{

        scanItem(itemId, "ItemScanned", "");
        emit ItemScanned(itemId, msg.sender);
    }

    /**
        Registers timestamp of action on item
     */
    function scanItem(uint256 itemId
                    ,string memory _event
                    ,string memory _data) private{

        //check valid request
        ItemRequest memory request = itemsToRequest[itemId];
        require(request.valid
                ,"Request not valid!");

        //saves scanned item to member, timestamp
        uint256 timestamp = block.timestamp;
        itemsToTrackingData[itemId].push(
            TrackingData({
                member: msg.sender
                ,timestamp: timestamp
                ,_event: _event
                ,attachment: _data
        }));
    }

    function getTrackingData(uint256 itemId) public view returns (TrackingData[] memory){

        return itemsToTrackingData[itemId];
    }

    function sayHelloMember() public view isMember returns(string memory){

        return "Hello Member!";
    }
}
