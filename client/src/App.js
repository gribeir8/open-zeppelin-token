import logo from './logo.svg';
import './App.css';
import React from "react";
import { DrizzleContext } from "@drizzle/react-plugin";
import ReadString from "./ReadString";
import SetString from "./SetString";



class App extends React.Component {

  render() {

    return (

      <DrizzleContext.Consumer>
        {drizzleContext => {
          const { drizzle, drizzleState, initialized } = drizzleContext;
    
          if (!initialized) {
            return "Loading...";
          }
    
          return (

            <div className="App">
              <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
      
                <ReadString
                  drizzle={drizzle}
                  drizzleState={drizzleState}
                />
      
                <SetString
                  drizzle={drizzle}
                  drizzleState={drizzleState}
                />
              </header>
            </div>
          );
        }}
      </DrizzleContext.Consumer>
    );
  }
}

export default App;
