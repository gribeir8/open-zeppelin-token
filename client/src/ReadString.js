import React from "react";

class ReadString extends React.Component {

    state = { dataKey: null };

  componentDidMount() {
    const { drizzle, drizzleState } = this.props;
    console.log(drizzle);
    console.log(drizzleState);
    const contract = drizzle.contracts.Tracking;

    // let drizzle know we want to watch the `tracking_name` method
    const dataKey = contract.methods["tracking_name"].cacheCall();

    // save the `dataKey` to local component state for later reference
    this.setState({ dataKey });
  }

  render() {
    // get the contract state from drizzleState
    const { Tracking } = this.props.drizzleState.contracts;

    // using the saved `dataKey`, get the variable we're interested in
    const tracking_name = Tracking.tracking_name[this.state.dataKey];

    // if it exists, then we display its value
    return (

        <div>
            <p>Contract Name: {tracking_name && tracking_name.value}</p>
        </div>
        ); 
    }

}

export default ReadString;
