// migrations/2_deploy.js
const TrackingFactory = artifacts.require('TrackingFactory');

module.exports = async function (deployer) {

    await deployer.deploy(TrackingFactory);
};