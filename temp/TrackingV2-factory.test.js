// Load dependencies
const { expect } = require('chai');
// Import utilities from Test Helpers
const { BN, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');

// Load compiled artifacts
const TrackingFactory = artifacts.require('TrackingFactory');
const TrackingV2 = artifacts.require('TrackingV2');

// Start test block
contract('TrackingV2', function ([ owner, other ]) {

  beforeEach(async function () {
      
    this.factory = await TrackingFactory.new({ from: owner });
    //create a proxy from factory
    await this.factory.createTracking('Teste2');
    const list = await this.factory.getDeployedTrackers();
    //get first from list
    this.proxy = await TrackingV2.at(list[0]);
    console.log("tracking_name: " + await this.proxy.tracking_name());
  });

  it('say hello to member', async function () {

    await this.proxy.addMember(owner);
    
    await this.proxy.sayHelloMember2();
  });

});