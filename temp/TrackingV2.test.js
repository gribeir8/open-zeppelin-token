// test/Box.test.js
// Load dependencies
const { expect } = require('chai');
const { deployProxy, upgradeProxy } = require('@openzeppelin/truffle-upgrades');

// Load compiled artifacts
const Tracking = artifacts.require('Tracking');
const TrackingV2 = artifacts.require('TrackingV2');

// Start test block
contract('TrackingV2 (proxy)', function ([ owner, other ]) {

  beforeEach(async function () {

    this.proxy = await deployProxy(Tracking, ['Teste1'], {initializer: 'initialize'});
    console.log("tracking_name: " + await this.proxy.tracking_name());
    this.proxy = await upgradeProxy(this.proxy.address, TrackingV2, {initializer: 'initialize'});
  });

  it('say hello to member', async function () {

    await this.proxy.addMember(owner);
    
    await this.proxy.sayHelloMember2();
  });

});