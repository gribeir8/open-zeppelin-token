const { admin } = require('@openzeppelin/truffle-upgrades');
  // Use address of your Gnosis Safe
  const { gnosisSafe } = require('../secrets.json');
  console.log("gnosisSafe: " + gnosisSafe);
 
module.exports = async function (deployer, network) {
 
  if (network == 'rinkeby') {
    
    console.log("is rinkeby!");
    // The owner of the ProxyAdmin can upgrade our contracts
    await admin.transferProxyAdminOwnership(gnosisSafe);

  } else if(network == 'development'){

    console.log("is development!");    
    //does nothing
  }
};