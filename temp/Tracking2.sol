// contracts/Box.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.3;

import "@openzeppelin/contracts/proxy/Clones.sol";
/**
    Clones Factory MUST alwais be put on last class version
    otherwise must migrate new version, and anyone will be able to create new
 */
contract TrackingFactory{
    
    address private implementation;
    address[] public deployedTrackers;

    constructor(){

        implementation = address(new TrackingV2());
    }

    function createTracking(string memory name) public {
        
        //minimal proxy
        address proxyClone = Clones.clone(implementation);
        Tracking tracker = Tracking(proxyClone);
        tracker.initialize(name);
        tracker.transferOwnership(msg.sender);
        deployedTrackers.push(proxyClone);
        
    }
    
    function getDeployedTrackers() public view returns (address[] memory){
        
        return deployedTrackers;
    }
}

import "./Tracking.sol";
contract TrackingV2 is Tracking {

    function sayHelloMember2() public view isMember returns(string memory){

        return "Hello Member!";
    }
}