# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
upgradable contract sample
reference:
https://forum.openzeppelin.com/t/openzeppelin-upgrades-step-by-step-tutorial-for-truffle/3579


### How do I get set up? ###

* Summary of set 
npm install
npx truffle compile --all

* Configuration
create a secrets.json with entries:
{
    "mnemonic": "",
    "infuraApiKey": ""
    ,"gnosisSafe": ""
    ,"pinataAPIKey": ""
    ,"pinataAPISecret": ""
    ,"privateKey": ""
  }

in case already don't have a gnosis safe:
https://help.gnosis-safe.io/en/articles/3876461-create-a-safe

* Dependencies
* Database configuration
* How to run tests
npx ganache-cli --deterministic
npx truffle test


* Deployment instructions
*local development
npx ganache-cli --deterministic
npx truffle migrate --network development
npx truffle console --network development
truffle(development)> const box = await Box.deployed();
truffle(development)> const box2 = await BoxV2.deployed();
truffle(development)> await box.store(42);
truffle(development)> await box.retrieve();
truffle(development)> await box2.retrieve();
truffle(development)> await box2.increment()
truffle(development)> await box.retrieve();

*rinkeby network
copy file 5_transfer_ownership_gnosis_safe.js from /test folder into /migrations.
  it will be necessary to run only once.
npx truffle migrate --network rinkeby --reset
npx truffle console --network 
truffle(rinkeby)> const box = await Box.deployed()
truffle(rinkeby)> const box2 = await BoxV5.deployed()
truffle(rinkeby)> await box.store(42)
truffle(rinkeby)> await box.retrieve()
truffle(rinkeby)> box.address
truffle(rinkeby)> box2.address

go to:
https://gnosis-safe.io/app/#/welcome
create safe on rinkeby network
click: apps -> open zeppelin
put both addresses in order to upgrade contract
aprove transaction on MetaMask

ctrl^C
npx truffle console --network
truffle(rinkeby)> const box = await Box.deployed()
truffle(rinkeby)> const box2 = await BoxV2.at(box.address);
truffle(rinkeby)> await box2.retrieve()
truffle(rinkeby)> await box2.increment()
truffle(rinkeby)> await box.retrieve();

### TESTANDO CONTRATO

npx truffle develop
compile
test


### TESTANDO SCRIPT

npx truffle develop
migrate
//em outro terminal:
npx truffle exec ./scripts/index-tracking.js 

### TESTANDO REACT

npx truffle develop
migrate
//em outro terminal:
cd client
npm start
//build produção
npm run build

### UPLOAD TO PINATA

sample:
node scripts/uploadToPinata.js temp/smiley.jpg

node scripts/uploadToPinata.js temp/NFT.json

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact