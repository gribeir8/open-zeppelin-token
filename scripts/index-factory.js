module.exports = async function main (callback) {
    try {
      // Our code will go here
  
        // Retrieve accounts from the local node
        const accounts = await web3.eth.getAccounts();
        console.log(accounts);

        // Set up a Truffle contract, representing our deployed factory
        const TrackingFactory = artifacts.require('TrackingFactory');
        const TrackingV2 = artifacts.require('TrackingV2');
        const factory = await TrackingFactory.deployed();

        //create a proxy from factory
        await factory.createTracking('Teste2').then( (result)=>{
          console.log(result.receipt.rawLogs[0]);
        });
        const list = await factory.getDeployedTrackers();
        console.log(list);
        //gets last from list
        const proxy = await TrackingV2.at(list[list.length-1]);

        //call function on 
        console.log("tracking_name: " + await proxy.tracking_name());
        await proxy.addMember('Member1', accounts[0]);
        const message = await proxy.sayHelloMember2();
        console.log(message);

        callback(0);
    } catch (error) {
      console.error(error);
      callback(1);
    }
  };