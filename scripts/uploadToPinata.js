
const { pinataAPIKey, pinataAPISecret } = require('../secrets.json');
const axios = require("axios");
const fs = require("fs");
const FormData = require("form-data");

//usage: node uploadToPinata.js <path-to-file>
const pinFileToIPFS = async () => {

  const url = `https://api.pinata.cloud/pinning/pinFileToIPFS`;
  let fileArg = process.argv[2];
  console.log("uploading file: " + fileArg);

  let data = new FormData();
  data.append("file", fs.createReadStream(fileArg));

  const res = await axios.post(url, data, {

    maxContentLength: "Infinity", 
    headers: {
      "Content-Type": `multipart/form-data; boundary=${data._boundary}`,
      pinata_api_key: pinataAPIKey, 
      pinata_secret_api_key: pinataAPISecret,
    },
  });
  console.log(res.data);
};


pinFileToIPFS();
