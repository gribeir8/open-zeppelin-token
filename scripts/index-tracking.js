module.exports = async function main (callback) {
    try {
      // Our code will go here
  
        // Retrieve accounts from the local node
        const accounts = await web3.eth.getAccounts();
        console.log(accounts);

        const Tracking = artifacts.require('Tracking');
        const proxy = await Tracking.deployed();

        //call function on 
        console.log("tracking_name: " + await proxy.tracking_name());
        await proxy.addMember('Member1', accounts[0]);
        const message = await proxy.sayHelloMember();
        console.log(message);

        callback(0);
    } catch (error) {
      console.error(error);
      callback(1);
    }
  };