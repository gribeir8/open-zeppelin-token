// test/Box.test.js
// Load dependencies
const { expect } = require('chai');
const { deployProxy, upgradeProxy } = require('@openzeppelin/truffle-upgrades');

// Import utilities from Test Helpers
const { BN, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');

// Load compiled artifacts
const Tracking = artifacts.require('Tracking');

// Start test block
contract('Tracking (proxy)', function ([ owner, other, another ]) {

  before(async function () {

    this.proxy = await deployProxy(Tracking, ['Teste1'], {initializer: 'initialize'});
    console.log("tracking_name: " + await this.proxy.tracking_name());
  });

  describe('testing all features', async () => {

    it('non member cannot call', async function () {

      // Test a transaction reverts
      await expectRevert(
  
        this.proxy.sayHelloMember(),
        'Tracking: Not a member!',
      );
    });
  
    let requestId;
    it('creates a request', async function (){
  
      //create request
      let receipt = await this.proxy.createRequest('Nescau', 'Requisição de compra de Nescau junto à Nestlé.', 'metadata/0');
      let requests = await this.proxy.getRequests();
      requestId = requests.length-1;
      console.log('request: ' + requests[0].metadata);
      expectEvent(receipt, 'RequestCreated', { id: new BN(requestId), name: 'Nescau'});
      expect(requests[0].name).to.be.equal('Nescau');
    });

    it('non owner cannot create request', async function (){
  
      // Test a transaction reverts
      await expectRevert(

        this.proxy.createRequest('nada', 'nada', '  '.trim(), { from: other })
        ,'Ownable: caller is not the owner'
      );      
    });

    let item;
    it('mints items', async function (){

      //must be member
      await this.proxy.addMember('Owner', owner);

      //mint 2 items
      let receipt = await this.proxy.createItem(requestId, '   '.trim());
      receipt = await this.proxy.createItem(requestId, 'metadata/2');
      //check last created/minted
      const requests = await this.proxy.getRequests();
      const items = requests[requestId].items;
      item = items[items.length-1];
      console.log('item: ' + await this.proxy.tokenURI(item));
      expectEvent(receipt, 'ItemCreated', { requestId: new BN(requestId), itemId: item});
    });

    it('adds other member', async function () {
  
      let name = 'Other';
      //must be member
      await this.proxy.addMember(name, other);
      
      console.log(await this.proxy.sayHelloMember({ from: other }));
      let member = await this.proxy.getCurrentMember({ from: other});
      //console.log("member: " + member);
      expect(member.name).to.be.equal(name);
      expect(this.proxy.members(other));
    });

    it('cannot add member twice', async function () {
  
      // Test a transaction reverts
      await expectRevert(

        this.proxy.addMember('Other', other)
        ,'Member already added!'
      );
    });

    it('Non item owner cannot transfer', async function (){
  
      // Test a transaction reverts
      await expectRevert(

        this.proxy.transferItem(item, owner, {from: other})
        ,'ERC721: transfer caller is not owner nor approved'
      );      
    });

    it('transfers item to other', async function (){
  
      //scan item, by member with timestamp
      let receipt = await this.proxy.transferItem(item, other);

      expectEvent(receipt, 'ItemTransfered', { itemId: item
                                            ,from: owner
                                            ,to: other });
  
    });

    it('Check tracking of item transfer', async function (){

      util.proxy = this.proxy;
      await util.checkTrackingData(item);
    });

    const util =  {
      
      proxy: undefined
      ,checkTrackingData: async function (itemId){

        const listOfScans = await this.proxy.getTrackingData(itemId);
        let trackData = listOfScans[listOfScans.length-1]; 
        //does the same using solidity getter (must pass index)
        trackData = await this.proxy.itemsToTrackingData(itemId,listOfScans.length-1);
        //console.log(trackData);
        console.log('member: ' + trackData.member);
        const date = new Date(trackData.timestamp*1000);//to unix timestamp
        console.log('timestamp: ' + date.toISOString());
        console.log('event: ' + trackData._event)
        console.log('metadata: ' + trackData.attachment);
      }
    }
    
    it('cannot transfer to non member', async function (){
  
      // Test a transaction reverts
      await expectRevert(

        this.proxy.transferItem(item, another, { from: other})
        ,'New owner not a member!'
      );      
    });

    it('adds metadata to item', async function (){

      let metadata = "metadata/4";
      let receipt = await this.proxy.addMetadata(item, metadata, { from: other});
      console.log(await this.proxy.getMetadata(item));
      expectEvent(receipt, 'MetadataAdded', { itemId: item, metadata: metadata});
    });

    it('Check tracking of added metadata', async function (){
  
      util.proxy = this.proxy;
      await util.checkTrackingData(item);
    });

    it('cannot add empty string as metadata', async function (){
  
      let metadata = "   ";

      // Test a transaction reverts
      await expectRevert(

        //MUST trim string
        this.proxy.addMetadata(item, metadata.trim(), { from: other})
        ,'Metadata cannot be empty string!'
      );      
    });

    it('Non owner cannot add metadata to item', async function (){
  
      const requests = await this.proxy.getRequests();
      const items = requests[requestId].items;
      let metadata = "metadata/4";

      // Test a transaction reverts
      await expectRevert(

        //first item not scanned
        this.proxy.addMetadata(items[0], metadata, { from: other})
        ,'Member is not current owner of Item!'
      );      
    });

    it('Non item owner (member) scans item', async function (){
  
      const requests = await this.proxy.getRequests();
      const items = requests[requestId].items;

      let receipt = await this.proxy.scanItemByMember(items[0], { from: other})
      expectEvent(receipt, 'ItemScanned', { itemId: items[0], member: other});      
    });

    it('Check tracking of item scan', async function (){

      const requests = await this.proxy.getRequests();
      const items = requests[requestId].items;

      util.proxy = this.proxy;
      await util.checkTrackingData(items[0]);
    });

    it('returns all members to owner', async function (){

      let members = await this.proxy.getAllMembers();
      console.log(members);
    });

    it('non owner cannot get all members', async function (){
  
      // Test a transaction reverts
      await expectRevert(

        this.proxy.getAllMembers( { from: other })
        ,'Ownable: caller is not the owner'
      );      
    });

    it('invalid request cannot create item', async function (){
  
      //invalidate request
      this.proxy.invalidateRequest(requestId);

      // Test a transaction reverts
      await expectRevert(

        this.proxy.createItem(requestId, 'metadata/3')
        ,'Request not valid!'
      );      
    });

  });

});