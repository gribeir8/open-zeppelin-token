// test/Box.test.js
// Load dependencies
const { expect } = require('chai');
const { deployProxy, upgradeProxy } = require('@openzeppelin/truffle-upgrades');

// Import utilities from Test Helpers
const { BN, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');

// Load compiled artifacts
const Box = artifacts.require('Box');
const BoxV2 = artifacts.require('BoxV2');

// Start test block
contract('BoxV2 (proxy)', function ([ owner, other ]) {
  // Use large integers ('big numbers')
  const value = new BN('42');

  beforeEach(async function () {

    this.box = await deployProxy(Box, {initializer: 'initialize'});
    await this.box.store(value);
    this.box2 = await upgradeProxy(this.box.address, BoxV2, {initializer: 'initialize'});
  });

  it('can increment', async function() {

    let value2 = await this.box2.retrieve();
    console.log('value2: ' + value2);
    await this.box2.increment();

    expect(await this.box2.retrieve()).to.be.bignumber.equal(new BN(++value2));    
  })
});