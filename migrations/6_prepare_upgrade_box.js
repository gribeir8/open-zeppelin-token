const { upgradeProxy, prepareUpgrade } = require('@openzeppelin/truffle-upgrades');

const Box = artifacts.require('Box');
const BoxV2 = artifacts.require('BoxV2');

module.exports = async function (deployer, network) {

  const existing = await Box.deployed();

  if (network == 'rinkeby') {

    console.log("is rinkeby!");
    //wait for gnosis safe upgrade
    await prepareUpgrade(existing.address, BoxV2, { deployer, initializer: 'initialize' });

  } else if(network == 'development'){

    console.log("is development!");
    await upgradeProxy(existing.address, BoxV2, { deployer, initializer: 'initialize' });
  }
};
