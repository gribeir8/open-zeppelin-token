// migrations/3_deploy_upgradeable_box.js
const { deployProxy } = require('@openzeppelin/truffle-upgrades');

const Tracking = artifacts.require('Tracking');

module.exports = async function (deployer) {
  await deployProxy(Tracking, ['Merenda Escolar'], { deployer, initializer: 'initialize' });
};